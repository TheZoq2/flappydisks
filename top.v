// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input clk,

    output[7:0] pmod0,
    output[7:0] pmod2,
    output[7:0] pmod3,
    output[7:0] pmod4,
    output[7:0] pmod7,

    input[7:0] pmod1
);
    assign rst = pmod1[0];

    wire vsync, hsync, r, g, b, input1_btn, input2_btn;

    assign pmod7[0] = vsync;
    assign pmod7[1] = hsync;

    assign pmod7[4] = r;
    assign pmod7[5] = g;
    assign pmod7[6] = b;

    assign input1_btn = pmod1[3];
    assign input2_btn = pmod1[1];


    wire[7:0] dummy;
    wire[7:0] debug_out;

    assign debug_out = pmod4;

    main main
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , .__output({dummy, debug_out, pmod2, pmod3, hsync, vsync, r, g, b, pmod0[0]})
        , ._i_input1_unsync(input1_btn)
        , ._i_input2_unsync(input2_btn)
        , ._i_start_unsync(pmod1[2])
        );
endmodule

